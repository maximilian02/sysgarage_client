import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { CookieService } from 'ngx-cookie';

import { environment } from '../../environments/environment';

@Injectable()
export class ApiService {
  private base: any;

  constructor(private _http: Http, private cookieService: CookieService) {
    this.base = environment.api.base;
  }

  createAuthorizationHeader(headers: Headers) {
    const token = this.cookieService.get('token');
    console.log(token);
    headers.append('Content-Type', 'application/json')
    if(token) {
      headers.append('x-access-token', token);
    }
  }

  get(action) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this._http.get(this.base + action, {
      headers: headers
    });
  }

  post(action, data) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this._http.post(this.base + action, data, {
      headers: headers
    });
  }
}