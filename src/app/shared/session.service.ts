import { Injectable } from '@angular/core';
import { Http, Response, RequestOptionsArgs } from '@angular/http';

import { ApiService } from './api.service'
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie';

import { environment } from '../../environments/environment';

@Injectable()
export class SessionService {
  private userData: any;
  private isAuthenticated: boolean;
  private config: any;

  constructor(private cookieService: CookieService, private router: Router) {
    this.config = environment.api;
    this.isAuthenticated = !!this.cookieService.get('token');
  }

  public getIsAuthenticated(): boolean {
    if (!this.isAuthenticated) {
      this.router.navigate(['/login']);
      this.isAuthenticated = !!this.cookieService.get('token');
    }
    return this.isAuthenticated;
  }
}