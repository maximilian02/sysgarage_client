import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie';
import { ApiService } from '../shared/api.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

@Injectable()
export class SignupService {

  constructor(private apiService: ApiService) {}

  signup(data): Observable<any> {
    return this.apiService.post('client', data)
        .map(res => res.json());
  }
}