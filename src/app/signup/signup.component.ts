import { Component } from '@angular/core';
import { SignupService } from './signup.service';
import { Router } from '@angular/router';
import 'rxjs/Rx';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html'
})
export class SignupComponent {
  public user: any;
  public hasError: boolean;

  constructor(private signupService: SignupService, private router: Router) {
    this.user = {
      email: '',
      password: ''
    };
    this.hasError = false;
  }

  signup() {
    this.signupService.signup(this.user)
      .subscribe(
        data => {
          if(data.success) {
            this.router.navigate(['/home']);
          } else {
            this.hasError = true;
          }
        },
        err => {
          this.hasError = true;
          console.log(err)
        }
      );
  }

  help() {
    alert(`Well, we can't help you since this is a test application. Come back later.`);
  }
}
