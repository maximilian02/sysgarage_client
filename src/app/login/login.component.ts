import { Component } from '@angular/core';
import { LoginService } from './login.service';
import { Router } from '@angular/router';
import 'rxjs/Rx';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  public user: any;
  public hasError: boolean;

  constructor(private loginService: LoginService, private router: Router) {
    this.user = {
      email: '',
      password: ''
    };
    this.hasError = false;
  }

  login() {
    this.loginService.login(this.user)
      .subscribe(
        data => {
          if(data.success) {
            this.router.navigate(['/home']);
          } else {
            this.hasError = true;
          }
        },
        err => {
          this.hasError = true;
          console.log(err)
        }
      );
  }

  help() {
    alert(`Well, we can't help you since this is a test application. Come back later.`);
  }
}
