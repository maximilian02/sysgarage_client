import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie';
import { ApiService } from '../shared/api.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

@Injectable()
export class LoginService {

  constructor(private apiService: ApiService, private cookieService: CookieService) {}

  login(data): Observable<any> {
    return this.apiService.post('authenticate', data)
        .map(res => {
            const data = res.json();
            if(data.success) {
                this.cookieService.put('token', data.token);
            }
            return data;
        });
  }
}