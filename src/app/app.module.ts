import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CookieModule } from 'ngx-cookie';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SignupComponent } from './signup/signup.component';

import { ApiService } from './shared/api.service'
import { SessionService } from './shared/session.service'
import { LoginService } from './login/login.service';
import { AuthGuard } from './shared/auth.guard';

import { routing } from './app.routing';



@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    CookieModule.forRoot(),
    routing
  ],
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    DashboardComponent
  ],
  providers: [ ApiService, SessionService, AuthGuard, LoginService],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
